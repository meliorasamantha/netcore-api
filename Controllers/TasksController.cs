// Controllers/TasksController.cs
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NETCOREAPI.Models;

namespace NETCOREAPI.Controllers
{
    [Route("api/tasks")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private static List<User> users = new List<User>();

        // POST: api/tasks/AddUserWithTask
        [HttpPost("AddUserWithTask")]
        public IActionResult AddUserWithTask([FromBody] User newUser)
        {
            newUser.UserId = users.Count + 1;
            users.Add(newUser);
            return CreatedAtAction(nameof(GetUserWithTask), new { name = newUser.Name }, newUser);
        }

        // GET: api/tasks/GetUserWithTask
        [HttpGet("GetUserWithTask")]
        public IActionResult GetUserWithTask(string name)
        {
            var user = users.Find(u => u.Name.ToLower() == name.ToLower());
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
    }
}
