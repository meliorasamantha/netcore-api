public class User
{
    public int UserId { get; set; }
    public string Name { get; set; }
    public List<Task> Tasks { get; set; }
}

